1. Installing
This theme is sub theme of fusion theme, so you first download fusion theme at http://drupal.org/project/fusion. and then extract Alphorn package in fusion folder. To use all features of Alphorn theme and our install profile package, please go to http://www.symphonythemes.com/drupal-theme/alphorn to download full site package. With few instalation steps, you will have a new site similar Alphorn.
1.1 Installing a new theme is straightforward:
1.	Download a new theme package and fusion theme.
2.	Read any README or INSTALL files in the package to find out if there are any special steps needed for this theme.
3.	Upload the contents of the theme package to a new directory in the themes directory in your Drupal site (/sites/all/themes/fusion/alphorn)
4.	Click administer � themes and enable the new theme (Drupal will auto-detect its presence).
5.	Edit your user preferences and select the new theme. If you want it to be the default theme for all users, check the default box in the themes administration page. 
2. Config block: At configuration page of block. Add add "block-list" class for block list or menu on side bar. Add "block-box" class for black block. Add "block-box block-login" class for login block. Add class "block-slideshow" for slideshow block. Add "block-latest-news" class for latest news block on home page.
